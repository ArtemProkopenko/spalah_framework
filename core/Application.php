<?php

namespace core;

use core\components\Request;

class Application
{
    public $id;

    public $baseDir;

    public $baseController;
    
    public $errorAction;

    protected $request;

    /**
     * @param array $config
    */
    public function __construct($config = [])
    {
        foreach ($config as $key => $param) {
            $this->{$key} = $param;
        }
        $this->request = new Request();
    }

    /**
     * @return integer
    */
    public function run()
    {
        if($controllerNamespace = $this->request->getControllerNamespace()){
            if(class_exists($controllerNamespace)){
                $controller = new $controllerNamespace();
                if($controllerAction = $this->request->getControllerAction()){
                    if(method_exists($controller, $controllerAction)){
                        $controller->$controllerAction();
                    }else{
                        $this->errorActionLaunch();
                    }
                }else{
                    $controller->{'action'.ucfirst($controller->defaultAction)}();
                }
            }else{
                $this->errorActionLaunch();
            }
        }else{
            $controller = new $this->baseController;
            $controller->{'action'.ucfirst($controller->defaultAction)}();
        }
        
        echo 'run application';
        return 1;
    }
    
    protected function errorActionLaunch()
    {
        $arr = explode('/', ltrim($this->errorAction, "/"));
        $controller = "\\controllers\\".ucfirst($arr[0])."Controller";
        $action = "action".ucfirst($arr[1]);
        $controller = new $controller;
        return $controller->{$action}();
        
    }
}