<?php

namespace core\components;

class Request
{
    protected $uri;

    public function __construct()
    {
        $this->uri = $_SERVER['REQUEST_URI'];
    }

    /**
     * @return string
    */
    public function getControllerNamespace()
    {
        $arr = explode('/', ltrim($this->uri, "/"));
        return !empty($arr[0]) ? "\\controllers\\".ucfirst($arr[0])."Controller" : '';
    }

    /**
     * @return string
     */
    public function getControllerAction()
    {
        $arr = explode('/', ltrim($this->uri, "/"));
        return !empty($arr[1]) ? "action".ucfirst($arr[1]) : '';
    }

}