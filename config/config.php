<?php

return [
    'id' => 'spalah_framework',
    'baseDir' => __DIR__ . '/spalah_framework',
    'baseController' => '\\controllers\\SiteController',
    'errorAction' => 'site/error'
];